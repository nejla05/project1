const express = require("express");
const bodyParser = require("body-parser");
require("./src/sequelize/db");
const routes = require("./src/routes/index");

const app = express();

app.use(bodyParser.json());
app.use(bodyParser.text());
app.use(bodyParser.urlencoded({ extended: true }));

app.use("/", routes);
app.use(express.static("./public/html"));
app.use("/resources", express.static("./public"));

app.listen("3000");

module.exports = app;
