const Grupa = require("../sequelize/db").Grupa;
const Student = require("../sequelize/db").Student;
const Vjezba = require("../sequelize/db").Vjezba;
const Zadatak = require("../sequelize/db").Zadatak;
const Sequelize = require("sequelize");
var op = Sequelize.Op;

const getVjezbe = async (req, res) => {
  try {
    const vjezbe = await Vjezba.findOne({
      where: {},
    });
    const zadaci = await Zadatak.findAll({
      where: {
        vjezba: vjezbe.naziv,
      },
      order: [["redniBroj", "ASC"]],
    });
    const response = {
      brojVjezbi: vjezbe.brojVjezbi,
      brojZadataka: zadaci.map((el) => el.brojZadataka),
    };
    res.status(200).send(response);
  } catch (e) {
    return res.status(400).send({ message: "Greska u GET vjezbe" });
  }
};

const postVjezbe = async (req, res) => {
  try {
    const brojVjezbi = !Number.isInteger(req.body.brojVjezbi)
      ? Number(req.body.brojVjezbi)
      : req.body.brojVjezbi;
    const brojZadataka = [];
    req.body.brojZadataka.forEach((element) => {
      const elToPush = !Number.isInteger(element) ? Number(element) : element;
      brojZadataka.push(elToPush);
    });

    // provjera unosa

    let errorText = "";

    if (
      !Number.isInteger(brojVjezbi) ||
      brojVjezbi < 1 ||
      brojVjezbi > 15 ||
      brojVjezbi !== brojZadataka.length
    )
      errorText += "brojVjezbi";
    for (let i = 0; i < brojZadataka.length; i++) {
      if (
        !Number.isInteger(brojZadataka[i]) ||
        brojZadataka[i] < 0 ||
        brojZadataka[i] > 10
      ) {
        if (errorText !== "") errorText = errorText + ",z" + i.toString();
        else errorText = errorText + "z" + i.toString();
      }
    }

    // response

    if (errorText !== "")
      res.status(400).send("Pogrešan parametar " + errorText);
    else {
      //Unistiti Prosle vjezbe
      await Vjezba.destroy({
        where: {},
        truncate: true,
      });
      await Zadatak.destroy({
        where: {},
        truncate: true,
      });

      //Kreirati Nove
      const promise = [];
      for (let i = 0; i < brojVjezbi; i++) {
        const string = `Vjezba${i + 1}`;
        promise.push(
          Vjezba.create({
            brojVjezbi: brojVjezbi,
            naziv: string,
          })
        );
        let zIndex = 0;
        for (const z of brojZadataka) {
          zIndex++;
          promise.push(
            Zadatak.create({
              brojZadataka: z,
              vjezba: string,
              redniBroj: zIndex,
            })
          );
        }
      }
      await Promise.all(promise);
      res.status(200).send({ brojVjezbi, brojZadataka });
    }
  } catch (e) {
    console.log(e);
    return res.send({ message: "Greska u POST Vjezbe" });
  }
};

const postStudent = async (req, res) => {
  try {
    const { ime, prezime, index, grupa } = req.body;
    if (!ime || !prezime || !index || !grupa)
      throw new Error("Potrebni atributi su ime, prezime, index, grupa");
    const studentFind = await Student.findOne({
      where: {
        index,
      },
    });

    if (studentFind) {
      return res.send({ status: `Student sa indexom ${index} već postoji!` });
    }

    let grupaFind = await Grupa.findOne({
      where: {
        naziv: grupa,
      },
    });

    if (!grupaFind) {
      grupaFind = await Grupa.create({
        naziv: grupa,
      });
    }
    await Student.create({
      ime,
      prezime,
      index,
      grupa,
    });

    return res.send({ status: "Kreiran student!" });
  } catch (e) {
    return res.status(400).send(e.message);
  }
};

const putStudent = async (req, res) => {
  try {
    const { index } = req.params;
    const { grupa } = req.body;
    if (!index) throw new Error("Potreban index");
    if (!grupa) throw new Error("Potrebna grupa");
    const studentFind = await Student.findOne({
      where: {
        index,
      },
    });

    if (!studentFind) {
      return res.send({ status: `Student sa indexom ${index} ne postoji` });
    }

    let grupaFind = await Grupa.findOne({
      where: {
        naziv: grupa,
      },
    });

    if (!grupaFind) {
      grupaFind = await Grupa.create({
        naziv: grupa,
      });
    }

    studentFind.grupa = grupa;
    await studentFind.save();

    return res.send({ status: `Promjenjena grupa studentu ${index}` });
  } catch (e) {
    return res.status(400).send(e.message);
  }
};

const sleep = (ms) => {
  return new Promise((resolve) => {
    setTimeout(resolve, ms);
  });
};

const kreairajStudente = async (studenti) => {
  try {
    let dodanih = 0;
    let indexiOdOnihKojiNisuDodani = [];
    for (const student of studenti) {
      const { ime, prezime, index, grupa } = student;
      const studentFind = await Student.findOne({
        where: {
          index,
        },
      });

      if (studentFind) {
        indexiOdOnihKojiNisuDodani.push(index);
      } else {
        let grupaFind = await Grupa.findOne({
          where: {
            naziv: grupa,
          },
        });

        if (!grupaFind) {
          grupaFind = await Grupa.create({
            naziv: grupa,
          });
        }
        await Student.create({
          ime,
          prezime,
          index,
          grupa,
        });
        dodanih++;
        //Sacekati 0.25 sec da bi sljedeci prolaz for petlje find one nasao
        await sleep(250);
      }
    }
    return {
      dodanih,
      indexiOdOnihKojiNisuDodani,
    };
  } catch (e) {
    return { dodan: false, indexiOdOnihKojiNisuDodani: null };
  }
};

const postBatchStudent = async (req, res) => {
  if (req.file) postBatchStudentPrekoFormData(req, res);
  else {
    postBatchStudentPrekoBodyText(req, res);
  }
};

const postBatchStudentPrekoBodyText = async (req, res) => {
  try {
    let N = 0;
    let indexiOdOnihKojiNisuDodani = [];
    const rows = req.body.split("\n");
    const students = [];
    rows.forEach((row) => {
      const studentInfo = row.split(",");
      if (studentInfo.length === 4) {
        students.push({
          ime: studentInfo[0],
          prezime: studentInfo[1],
          index: studentInfo[2],
          grupa: studentInfo[3],
        });
      }
    });

    const rezultati = await kreairajStudente(students);
    if (rezultati.indexiOdOnihKojiNisuDodani === null)
      throw new Error("Pogresan format");
    N = rezultati.dodanih || 0;
    indexiOdOnihKojiNisuDodani = rezultati.indexiOdOnihKojiNisuDodani || [];
    if (indexiOdOnihKojiNisuDodani.length > 0) {
      let stringIndex = "";
      indexiOdOnihKojiNisuDodani.forEach((element, index) => {
        if (index === indexiOdOnihKojiNisuDodani.length - 1)
          stringIndex += `${element}`;
        else stringIndex += `${element},`;
      });
      return res.send({
        status: `Dodano ${N} studenata, a studenti ${stringIndex} već postoje!`,
      });
    }
    return res.send({ status: `Dodano ${N} studenata!` });
  } catch (e) {
    return res.status(400).send(e.message);
  }
};

const postBatchStudentPrekoFormData = async (req, res) => {
  try {
    let N = 0;
    let indexiOdOnihKojiNisuDodani = [];
    if (req.file.mimetype !== "text/csv")
      throw new Error("File mora biti text/csv tipa");
    const bufferString = req.file.buffer.toString("utf8");
    const rows = bufferString.split("\n");
    const students = [];
    rows.forEach((row) => {
      const studentInfo = row.split(",");
      if (studentInfo.length === 4) {
        students.push({
          ime: studentInfo[0],
          prezime: studentInfo[1],
          index: studentInfo[2],
          grupa: studentInfo[3].replace("\r", ""),
        });
      }
    });

    const rezultati = await kreairajStudente(students);
    if (rezultati.indexiOdOnihKojiNisuDodani === null)
      throw new Error("Pogresan format");
    N = rezultati.dodanih || 0;
    indexiOdOnihKojiNisuDodani = rezultati.indexiOdOnihKojiNisuDodani || [];
    if (indexiOdOnihKojiNisuDodani.length > 0) {
      let stringIndex = "";
      indexiOdOnihKojiNisuDodani.forEach((element, index) => {
        if (index === indexiOdOnihKojiNisuDodani.length - 1)
          stringIndex += `${element}`;
        else stringIndex += `${element},`;
      });
      return res.send({
        status: `Dodano ${N} studenata, a studenti ${stringIndex} već postoje!`,
      });
    }
    return res.send({ status: `Dodano ${N} studenata!` });
  } catch (e) {
    return res.status(400).send(e.message);
  }
};

module.exports = {
  getVjezbe,
  postVjezbe,
  postStudent,
  putStudent,
  postBatchStudent,
};
