const fs = require("fs");
const path = require("path");

const folder = __dirname + "/../../";

const getVjezbe = async (req, res) => {
  try {
    let data = fs
      .readFileSync(path.resolve(folder + "/vjezbe.csv"), "utf8")
      .split(",");
    const brojVjezbi = data[0];
    data.shift();
    const response = {
      brojVjezbi: brojVjezbi,
      brojZadataka: data,
    };
    res.status(200).send(response);
  } catch (e) {
    console.log(e);
    return res.status(400).send({ message: "Greska u GET vjezbe" });
  }
};

const postVjezbe = async (req, res) => {
  try {
    const brojVjezbi = !Number.isInteger(req.body.brojVjezbi)
      ? Number(req.body.brojVjezbi)
      : req.body.brojVjezbi;
    const brojZadataka = [];
    req.body.brojZadataka.forEach((element) => {
      const elToPush = !Number.isInteger(element) ? Number(element) : element;
      brojZadataka.push(elToPush);
    });

    // provjera unosa

    let errorText = "";

    if (
      !Number.isInteger(brojVjezbi) ||
      brojVjezbi < 1 ||
      brojVjezbi > 15 ||
      brojVjezbi !== brojZadataka.length
    )
      errorText += "brojVjezbi";
    for (let i = 0; i < brojZadataka.length; i++) {
      if (
        !Number.isInteger(brojZadataka[i]) ||
        brojZadataka[i] < 0 ||
        brojZadataka[i] > 10
      ) {
        if (errorText !== "") errorText = errorText + ",z" + i.toString();
        else errorText = errorText + "z" + i.toString();
      }
    }

    // response

    if (errorText !== "")
      res.status(400).send("Pogrešan parametar " + errorText);
    else {
      const requestInput =
        brojVjezbi.toString() + "," + brojZadataka.toString();
      fs.writeFile(
        path.resolve(folder + "/vjezbe.csv"),
        requestInput,
        function (err) {
          if (err) throw new Error("Write file err");
        }
      );
      res.status(200).send({ brojVjezbi, brojZadataka });
    }
  } catch (e) {
    console.log(e);
    return res.send({ message: "Greska u POST Vjezbe" });
  }
};

module.exports = {
  getVjezbe,
  postVjezbe,
};
