const express = require("express");
const controllerDB = require("../controllers/spirala4");
const multer = require("multer");
const storage = multer.memoryStorage();
const upload = multer({ storage: storage });

const router = express.Router();

router.route("/vjezbe").get(controllerDB.getVjezbe);

router.route("/vjezbe").post(controllerDB.postVjezbe);

router.route("/student").post(controllerDB.postStudent);

router.route("/student/:index").put(controllerDB.putStudent);

router
  .route("/batch/student")
  .post(upload.single("file"), controllerDB.postBatchStudent);

module.exports = router;
