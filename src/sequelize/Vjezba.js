module.exports = (sequelize, type) => {
  return sequelize.define("Vjezba", {
    naziv: {
      type: type.STRING,
      allowNull: false,
      unique: true,
    },
    brojVjezbi: {
      type: type.STRING,
      allowNull: false,
    }
  });
};
