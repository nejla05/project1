module.exports = (sequelize, type) => {
  return sequelize.define("Student", {
    ime: {
      type: type.STRING,
      allowNull: false,
      unique: false,
    },
    prezime: {
      type: type.STRING,
      allowNull: false,
      unique: false,
    },
    index: {
      type: type.STRING,
      allowNull: false,
      unique: true,
    },
    grupa: {
      type: type.STRING,
      allowNull: false,
      unique: false,
    },
  });
};
