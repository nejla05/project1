module.exports = (sequelize, type) => {
  return sequelize.define("Zadatak", {
    brojZadataka: {
      type: type.STRING,
      allowNull: false,
    },
    vjezba: {
      type: type.STRING,
      allowNull: false,
    },
    redniBroj: {
      type: type.INTEGER,
      allowNull: false,
    }
  });
};
