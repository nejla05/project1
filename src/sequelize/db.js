const mysql = require("mysql2/promise");
const Sequelize = require("sequelize");
const grupa = require("./Grupa");
const student = require("./Student");
const vjezba = require("./Vjezba");
const zadatak = require("./Zadatak");

const initCheck = async () => {
  try {
    const connection = await mysql.createConnection({
      host: "localhost",
      port: 3306,
      user: "root",
      password: "password",
    });
    await connection.query(`CREATE DATABASE IF NOT EXISTS wt2118369;`);
  } catch (e) {
    console.log("Error in initCheck()", e);
  }
};

initCheck();

// const sequelize = new Sequelize('database', 'username', 'password', {
//   host: 'localhost',
//   dialect: /* one of 'mysql' | 'mariadb' | 'postgres' | 'mssql' */
// });

const sequelize = new Sequelize("wt2118369", "root", "password", {
  host: "localhost",
  dialect: "mysql",
  operatorsAliases: false,
  pool: {
    max: 5,
    min: 0,
    acquire: 30000,
    idle: 10000,
  },
  logging: false,
});

const Grupa = grupa(sequelize, Sequelize);
const Student = student(sequelize, Sequelize);
const Vjezba = vjezba(sequelize, Sequelize);
const Zadatak = zadatak(sequelize, Sequelize);

sequelize
  .sync({ force: false })
  .then(async () => {
    console.log("Database up");
  })
  .catch((err) => {
    console.log("Error", err);
  });

module.exports = {
  Grupa: Grupa,
  Student: Student,
  Vjezba: Vjezba,
  Zadatak: Zadatak,
};
