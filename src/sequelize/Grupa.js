module.exports = (sequelize, type) => {
  return sequelize.define("Grupa", {
    naziv: {
      type: type.STRING,
      allowNull: false,
      unique: true,
    },
  });
};
