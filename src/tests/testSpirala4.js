let chai = require("chai");
let chaiHttp = require("chai-http");
let app = require("../../index");
chai.use(chaiHttp);
let assert = chai.assert;

const Grupa = require("../sequelize/db").Grupa;
const Student = require("../sequelize/db").Student;
const Vjezba = require("../sequelize/db").Vjezba;
const Zadatak = require("../sequelize/db").Zadatak;

beforeEach(async () => {
  await new Promise((resolve) => setTimeout(resolve, 1500));
  console.log("----------------------");
});
describe("test", function () {
  describe("POST /student", async function () {
    it("Should pass - create", function () {
      let student = {
        ime: "A",
        prezime: "B",
        index: "3",
        grupa: "2a",
      };
      chai
        .request(app)
        .post("/student")
        .set("content-type", "application/json")
        .send(student)
        .end((err, res) => {
          assert.equal(res.body.status, "Kreiran student!", "Pass");
        });
    });

    it("Should pass - already created", function () {
      let student = {
        ime: "A",
        prezime: "B",
        index: "3",
        grupa: "2a",
      };
      chai
        .request(app)
        .post("/student")
        .set("content-type", "application/json")
        .send(student)
        .end((err, res) => {
          assert.equal(
            res.body.status,
            "Student sa indexom 3 već postoji!",
            "Pass"
          );
        });
    });

    it("Should pass - err 1", function () {
      let student = {
        ime: "A",
        prezime: "B",
      };
      chai
        .request(app)
        .post("/student")
        .set("content-type", "application/json")
        .send(student)
        .end((err, res) => {
          assert.equal(res.status, 400, "Pass");
        });
    });
  });

  describe("PUT /student/:index", async function () {
    it("Should pass - change", function () {
      chai
        .request(app)
        .put("/student/3")
        .set("content-type", "application/json")
        .send({
          grupa: "3a",
        })
        .end((err, res) => {
          assert.equal(res.body.status, "Promjenjena grupa studentu 3", "Pass");
        });
    });

    it("Should pass - change", function () {
      chai
        .request(app)
        .put("/student/5")
        .set("content-type", "application/json")
        .send({
          grupa: "3a",
        })
        .end((err, res) => {
          assert.equal(
            res.body.status,
            "Student sa indexom 5 ne postoji",
            "Pass"
          );
        });
    });

    it("Should drop items", async function () {
      await Grupa.destroy({
        where: {},
        truncate: true,
      });
      await Student.destroy({
        where: {},
        truncate: true,
      });
    });
  });

  describe("POST /batch/student", async function () {
    it("Should pass - change", function () {
      chai
        .request(app)
        .post("/batch/student")
        .set("content-type", "text/plain")
        .send(`NIme1,Nime2,123abc,Ngrupa1\nNIme1,Nime2,124abc,Ngrupa1`)
        .end((err, res) => {
          assert.equal(res.body.status, "Dodano 2 studenata!", "Pass");
        });
    });

    it("Should pass - change", function () {
      chai
        .request(app)
        .post("/batch/student")
        .set("content-type", "text/plain")
        .send(`NIme1,Nime2,123abc,Ngrupa1\nNIme1,Nime2,124abc,Ngrupa1`)
        .end((err, res) => {
          assert.equal(
            res.body.status,
            "Dodano 0 studenata, a studenti 123abc,124abc već postoje!",
            "Pass"
          );
        });
    });

    it("Should drop items", async function () {
      await Vjezba.destroy({
        where: {},
        truncate: true,
      });
      await Zadatak.destroy({
        where: {},
        truncate: true,
      });
    });
  });
});
