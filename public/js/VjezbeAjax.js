const dodajInputPolja = (DOMelementDIVauFormi, brojVjezbi) => {
  for (let i = 0; i < brojVjezbi; i++) {
    let tag = document.createElement("div");
    let labela = document.createElement("label");
    const tekst = document.createTextNode(`z${i} : `);
    let input = document.createElement("input");
    labela.id = `z${i}`;
    input.value = 4;
    labela.appendChild(tekst);
    tag.appendChild(labela);
    tag.appendChild(input);
    DOMelementDIVauFormi.appendChild(tag);
  }
};

const posaljiPodatke = async (vjezbeObjekat, callbackFunkcija) => {
  let requester = new Requester();
  const data = await requester._sendRequest(
    "POST",
    "/vjezbe",
    vjezbeObjekat,
    callbackFunkcija
  );
};

const dohvatiPodatke = async (callbackFunkcija) => {
  let requester = new Requester();
  const response = await requester._sendRequest(
    "GET",
    "/vjezbe",
    {},
    callbackFunkcija
  );
};

function iscrtajZadatke(vjezbaDOMelement, brojZadataka) {
  const zadaci =
    vjezbaDOMelement.childNodes[vjezbaDOMelement.childNodes.length - 1];
  if (zadaci.childNodes.length === 0) {
    if (brojZadataka === 0 || brojZadataka === "0") {
      return;
    }
    let zadaci = document.createElement("div");
    zadaci.className = "zadaci";
    for (let j = 0; j < brojZadataka; j++) {
      let zadatak = document.createElement("div");
      zadatak.className = "zadatak";
      const tekst1 = document.createTextNode(`ZADATAK ${j}`);
      zadatak.appendChild(tekst1);
      zadaci.appendChild(zadatak);
    }
    vjezbaDOMelement.appendChild(zadaci);
  } else {
    zadaci.childNodes.forEach((el) => {
      if (el.className === "zadatak") {
        if (el.style.display === "") el.style.display = "none";
        else if (el.style.display === "none") el.style.display = "";
      }
    });
  }
}

const iscrtajVjezbe = (divDOMelement, data) => {
  if (data.brojVjezbi < 1 || data.brojVjezbi > 15) return;
  for (let i = 0; i < data.brojVjezbi; i++) {
    let vjezbaTag = document.createElement("div");
    vjezbaTag.className = "vjezba";
    let vjezbaTitle = document.createElement("div");
    vjezbaTitle.className = "vjezbaTitle";
    vjezbaTag.id = `vjezba${i}id`;
    let title = document.createElement("p");
    const tekst = document.createTextNode(`VJEZBA ${i}`);
    title.appendChild(tekst);
    vjezbaTitle.appendChild(title);
    vjezbaTag.appendChild(vjezbaTitle);
    let zadaci = document.createElement("div");
    zadaci.className = "zadaci";
    zadaci.id = `zadaci${i}id`;
    for (let j = 0; j < data.brojZadataka[i]; j++) {
      let zadatak = document.createElement("div");
      zadatak.className = "zadatak";
      const tekst1 = document.createTextNode(`ZADATAK ${j}`);
      zadatak.appendChild(tekst1);
      zadaci.appendChild(zadatak);
    }
    vjezbaTag.appendChild(zadaci);
    divDOMelement.appendChild(vjezbaTag);
    vjezbaTag.addEventListener("click", function () {
      onClickDynamic(i, data.brojZadataka[i]);
    });
  }
};
