const dodajBatch = async (csvStudenti, fnCallback) => {
  let requester = new Requester();
  const response = await requester._sendRequestText(
    "POST",
    "/batch/student",
    csvStudenti,
    fnCallback
  );
};

const dodajStudenta = async (student, fnCallback) => {
  let requester = new Requester();
  const response = await requester._sendRequest(
    "POST",
    "/student",
    student,
    fnCallback
  );
};

const postaviGrupu = async (index, grupa, fnCallback) => {
  let requester = new Requester();
  const response = await requester._sendRequest(
    "PUT",
    `/student/${index}`,
    {
      grupa,
    },
    fnCallback
  );
};
