window.onload = () => {
  if (document.getElementById("forma") !== null) {
    document
      .getElementById("forma")
      .addEventListener("click", function (event) {
        event.preventDefault();
      });
  }
};

function submitCSV(event) {
  dodajBatch(event.path[1][0].value, callbackFja1);
}

function callbackFja1(e, res) {
  const div = document.getElementById("ajaxstatus");
  if (div) {
    div.innerHTML = `${res.status}`;
  } else {
    const body = document.getElementsByClassName("bodyStranice")[0];
    console.log("postaviGrupu done!", body);
    let divA = document.createElement("div");
    divA.id = "ajaxstatus";
    divA.innerHTML = `${res.status}`;
    body.appendChild(divA);
  }
}

function submitStudent(event) {
  const ime = document.getElementById("imeInput");
  const prezime = document.getElementById("prezimeInput");
  const index = document.getElementById("indexInput");
  const grupa = document.getElementById("grupaInput");
  const student = {
    ime: ime.value,
    prezime: prezime.value,
    index: index.value,
    grupa: grupa.value,
  };
  dodajStudenta(student, callbackFja2);
}

function callbackFja2(e, res) {
  const div = document.getElementById("ajaxstatus");
  if (div) {
    div.innerHTML = `${res.status}`;
  } else {
    const body = document.getElementsByClassName("bodyStranice")[0];
    console.log("postaviGrupu done!", body);
    let divA = document.createElement("div");
    divA.id = "ajaxstatus";
    divA.innerHTML = `${res.status}`;
    body.appendChild(divA);
  }
}

function submitPostaviGrupu(event) {
  const index = document.getElementById("indexInput");
  const grupa = document.getElementById("grupaInput");
  postaviGrupu(index.value, grupa.value, callbackFja3);
}

function callbackFja3(e, res) {
  const div = document.getElementById("ajaxstatus");
  if (div) {
    div.innerHTML = `${res.status}`;
  } else {
    const body = document.getElementsByClassName("bodyStranice")[0];
    console.log("postaviGrupu done!", body);
    let divA = document.createElement("div");
    divA.id = "ajaxstatus";
    divA.innerHTML = `${res.status}`;
    body.appendChild(divA);
  }
}
