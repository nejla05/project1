let assert = chai.assert;
describe("test", function () {
  describe("dodajInputPolja()", function () {
    it("Test 1, input 1", function () {
      const obj = document.getElementById("brojVjezbiInput");
      obj.value = 1;
      const objInputi = document.getElementById("inputiZaZadatke");
      dodajInputPolja(objInputi, 1);
      assert.equal(objInputi.childNodes.length, 1, "1 nov div kreiran");
      while (objInputi.firstChild) {
        objInputi.removeChild(objInputi.firstChild);
      }
    });

    it("Test 2, input 2", function () {
      const obj = document.getElementById("brojVjezbiInput");
      obj.value = 2;
      const objInputi = document.getElementById("inputiZaZadatke");
      dodajInputPolja(objInputi, 2);
      assert.equal(objInputi.childNodes.length, 2, "2 nova diva kreirana");
      while (objInputi.firstChild) {
        objInputi.removeChild(objInputi.firstChild);
      }
    });

    it("Test 3, input 3", function () {
      const obj = document.getElementById("brojVjezbiInput");
      obj.value = 3;
      const objInputi = document.getElementById("inputiZaZadatke");
      dodajInputPolja(objInputi, 3);
      assert.equal(objInputi.childNodes.length, 3, "3 nova diva kreirana");
      while (objInputi.firstChild) {
        objInputi.removeChild(objInputi.firstChild);
      }
    });

    it("Test 4, input 4", function () {
      const obj = document.getElementById("brojVjezbiInput");
      obj.value = 4;
      const objInputi = document.getElementById("inputiZaZadatke");
      dodajInputPolja(objInputi, 4);
      assert.equal(objInputi.childNodes.length, 4, "4 nova diva kreirana");
      while (objInputi.firstChild) {
        objInputi.removeChild(objInputi.firstChild);
      }
    });

    it("Test 5, input 5", function () {
      const obj = document.getElementById("brojVjezbiInput");
      obj.value = 5;
      const objInputi = document.getElementById("inputiZaZadatke");
      dodajInputPolja(objInputi, 5);
      assert.equal(objInputi.childNodes.length, 5, "5 novih divova kreirani");
      while (objInputi.firstChild) {
        objInputi.removeChild(objInputi.firstChild);
      }
    });
  });

  describe("Requester()", function () {
    it("Test 1", async function () {
      let requester = new Requester();
      let values = {};
      const callbackFja = (error, data) => {
        values = {
          error,
          data,
        };
        assert.equal(error, null, "No Error");
        assert.equal(data.brojVjezbi, 3, "Broj vjezbi 3");
        assert.equal(data.brojZadataka.length, 3, "Broj vjezbi 3");
      };
      await requester._sendRequest(
        "POST",
        "/vjezbe",
        {
          brojVjezbi: 3,
          brojZadataka: [1, 2, 3],
        },
        callbackFja
      );
    });

    it("Test 2", async function () {
      let requester = new Requester();
      let values = {};
      const callbackFja = (error, data) => {
        values = {
          error,
          data,
        };
        assert.equal(error, null, "No Error");
        assert.equal(data.brojVjezbi, 4, "Broj vjezbi 4");
        assert.equal(data.brojZadataka.length, 4, "Broj vjezbi 4");
      };
      await requester._sendRequest(
        "POST",
        "/vjezbe",
        {
          brojVjezbi: 4,
          brojZadataka: [1, 2, 3, 4],
        },
        callbackFja
      );
    });

    it("Test 3 - Should throw error", async function () {
      let requester = new Requester();
      let values = {};
      const callbackFja = (error, data) => {
        values = {
          error,
          data,
        };
        assert.equal(data, null, "No Data");
        assert.equal(error, "Pogrešan parametar brojVjezbi", "Got Error");
      };
      await requester._sendRequest(
        "POST",
        "/vjezbe",
        {
          brojVjezbi: 2,
          brojZadataka: [1, 2, 3, 4],
        },
        callbackFja
      );
    });

    it("Test 4 - Should throw error", async function () {
      let requester = new Requester();
      let values = {};
      const callbackFja = (error, data) => {
        values = {
          error,
          data,
        };
        assert.equal(data, null, "No Data");
        assert.equal(error, "Pogrešan parametar brojVjezbi", "Got Error");
      };
      await requester._sendRequest(
        "POST",
        "/vjezbe",
        {
          brojVjezbi: 16,
          brojZadataka: [1, 2, 3, 4, 4, 5, 6, 7, 8, 9, 1, 1, 2, 3, 4, 5, 6],
        },
        callbackFja
      );
    });

    it("Test 1 - GET", async function () {
      let requester = new Requester();
      let values = {};
      const callbackFja = (error, data) => {
        values = {
          error,
          data,
        };
        assert.equal(data.brojVjezbi, "4", "Data");
        assert.equal(error, null, "No Error");
      };
      const response = await requester._sendRequest(
        "GET",
        "/vjezbe",
        {},
        callbackFja
      );
    });
  });

  describe("iscrtajVjezbe()", function () {
    it("Test 1, [1]", function () {
      const obj = document.getElementById("odabirVjezbe");
      if (obj) {
        iscrtajVjezbe(obj, {
          brojVjezbi: "1",
          brojZadataka: ["1"],
        });
        assert.equal(obj.childNodes.length, 1, "Nacrtao Vjezbe 1, [1]");
        while (obj.firstChild) {
          obj.removeChild(obj.firstChild);
        }
      }
    });

    it("Test 3, [2,0,3]", function () {
      const obj = document.getElementById("odabirVjezbe");
      if (obj) {
        iscrtajVjezbe(obj, {
          brojVjezbi: "3",
          brojZadataka: ["2", "0", "3"],
        });

        assert.equal(obj.childNodes.length, 3, "Nacrtao Vjezbe 3, [2,0,3]");
        while (obj.firstChild) {
          obj.removeChild(obj.firstChild);
        }
      }
    });

    it("Test 2, [1,1]", function () {
      const obj = document.getElementById("odabirVjezbe");
      if (obj) {
        iscrtajVjezbe(obj, {
          brojVjezbi: "2",
          brojZadataka: ["1", "1"],
        });

        assert.equal(obj.childNodes.length, 2, "Nacrtao Vjezbe 2, [1,1]");
      }
    });
  });

  describe("iscrtajZadatke()", function () {
    it("Test display none", function () {
      const obj = document.getElementById("odabirVjezbe");
      if (obj) {
        iscrtajZadatke(obj.childNodes[0], 1);
        assert.equal(
          obj.childNodes[0].childNodes[1].childNodes[0].style.display ===
            "none",
          true,
          "Display none"
        );
      }
    });

    it("Test display not none", function () {
      const obj = document.getElementById("odabirVjezbe");
      if (obj) {
        iscrtajZadatke(obj.childNodes[0], 1);
        assert.equal(
          obj.childNodes[0].childNodes[1].childNodes[0].style.display ===
            "none",
          false,
          "Display not none"
        );
      }
    });
  });
});
