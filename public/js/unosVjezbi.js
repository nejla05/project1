window.onload = () => {
  if (document.getElementById("forma") !== null) {
    document
      .getElementById("forma")
      .addEventListener("click", function (event) {
        event.preventDefault();
      });
  }
};

function unosBrojaInputaMiddleware(e) {
  unosBrojaInputa(e);
}

function unosBrojaInputa(e) {
  if (e.key == "Enter") {
    const obj = document.getElementById("inputiZaZadatke");
    dodajInputPolja(obj, e.target.value);
  }
}

function submitFormMiddleware(e) {
  submitForm(e);
}

function submitForm(e) {
  const brojVjezbi = e.path[1][0].value;
  let brojZadataka = [];
  for (let i = 1; i <= brojVjezbi; i++) {
    brojZadataka.push(e.path[1][i].value);
  }
  const req = {
    brojVjezbi,
    brojZadataka,
  };
  posaljiPodatke(req, callbackFja);
}

function callbackFja(error, data) {
  console.log("Rezultat", error, data);
  if (data)
    alert(
      `Dodano ${data.brojVjezbi} vjezbi i brojevi zadataka su ${data.brojZadataka}`
    );
}
