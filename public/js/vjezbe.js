window.onload = () => {
  const obj = document.getElementById("odabirVjezbe");
  if (obj !== null) {
    dohvatiPodatke(callbackIscrtaj);
  }
};

function callbackIscrtaj(error, data) {
  if (error) return;
  const obj = document.getElementById("odabirVjezbe");
  if (!!!obj || obj == {}) {
    let odabirVjezbe = document.createElement("div");
    odabirVjezbe.id = "odabirVjezbe";
    document.appendChild(odabirVjezbe);
    iscrtajVjezbe(obj, odabirVjezbe);
  } else {
    iscrtajVjezbe(obj, data);
  }
}

function onClickDynamic(brojVjezbe, brojZadataka) {
  middleware(brojVjezbe, brojZadataka);
}

function middleware(brojVjezbe, brojZadataka) {
  const id = `vjezba${brojVjezbe}id`;
  const obj = document.getElementById(id);
  iscrtajZadatke(obj, brojZadataka);
}
