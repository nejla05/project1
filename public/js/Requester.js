var Requester = (function () {
  var konstruktor = function () {
    const API_URL = "http://localhost:3000";

    const _sendRequest = (method, uri, body, cb) => {
      const ajax = new XMLHttpRequest();
      ajax.onreadystatechange = () => {
        if (ajax.readyState == 4 && ajax.status == 200) {
          cb(null, JSON.parse(ajax.responseText));
        } else if (ajax.readyState == 4) {
          cb(ajax.responseText, null);
        }
      };
      ajax.open(method, API_URL + uri, true);
      ajax.setRequestHeader("Content-Type", "application/json");
      ajax.send(JSON.stringify(body));
    };

    const _sendRequestText = (method, uri, body, cb) => {
      const ajax = new XMLHttpRequest();
      ajax.onreadystatechange = () => {
        if (ajax.readyState == 4 && ajax.status == 200) {
          cb(null, JSON.parse(ajax.responseText));
        } else if (ajax.readyState == 4) {
          cb(ajax.responseText, null);
        }
      };
      ajax.open(method, API_URL + uri, true);
      ajax.setRequestHeader("Content-Type", "text/plain");
      ajax.send(body);
    };
    return {
      _sendRequest,
      _sendRequestText,
    };
  };
  return konstruktor;
})();
